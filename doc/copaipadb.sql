-- phpMyAdmin SQL Dump
-- version 3.5.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 20-11-2016 a las 21:06:10
-- Versión del servidor: 5.5.28-log
-- Versión de PHP: 5.4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `copaipadb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivos`
--

CREATE TABLE IF NOT EXISTS `archivos` (
  `id-proyecto` int(11) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL DEFAULT '0',
  `nombre` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estado` enum('') COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`version`,`id-proyecto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_proy`
--

CREATE TABLE IF NOT EXISTS `detalle_proy` (
  `id_proyecto` int(11) NOT NULL DEFAULT '0',
  `certificacion` int(11) NOT NULL DEFAULT '0',
  `subtarea` int(11) NOT NULL DEFAULT '0',
  `id_categoria` int(11) DEFAULT NULL,
  `cantidad` float DEFAULT NULL,
  `costo` float DEFAULT NULL,
  `arch` varchar(0) COLLATE utf8_spanish2_ci DEFAULT NULL COMMENT 'archivo subido',
  `est` enum('off','on') COLLATE utf8_spanish2_ci DEFAULT 'on',
  PRIMARY KEY (`subtarea`,`certificacion`,`id_proyecto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedad`
--

CREATE TABLE IF NOT EXISTS `novedad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id-proyecto` int(11) DEFAULT NULL COMMENT 'fk proyectos',
  `detalle` text COLLATE utf8_spanish2_ci,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE IF NOT EXISTS `pagos` (
  `idpago` int(11) NOT NULL DEFAULT '0',
  `id_proyecto` int(11) DEFAULT NULL COMMENT 'fk a proyectos',
  `monto` float DEFAULT NULL,
  `tipo` enum('cheque','tarjeta','efectivo') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `otros` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`idpago`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE IF NOT EXISTS `proyectos` (
  `idproyect` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `catastro` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `creacion` date DEFAULT NULL,
  `comitente` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `destino` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `total` float DEFAULT NULL,
  `est` enum('DEVOLUCION EXCEDE ALCANCE INCUMBENCIA','DEVOLUCION RES. COPAIPA Nº 67/2011','OBSERVADO - VERIFICADO PARA COPIAS','VERIFICADO FALTAN COPIAS Y/O FIRMA','NO CUMPLE REGLAMENTACIÓN - RETIRAR PARA CORRECCIÓN','OBSERVADO - RETIRAR PARA CORRECCIÓN','DEVOLUCION CORRESPONDE TRAMITE MUNICIPAL','VERIFICADO - CONFORME A OBRA','VERIFICADO - RELEVAMIENTO','VERIFICADO - CONDICIONES MINIMAS','VERIFICADO - CUMPLE LEY','PENDIENTE DE VERIFICACIÓN') COLLATE utf8_spanish2_ci DEFAULT 'PENDIENTE DE VERIFICACIÓN',
  PRIMARY KEY (`idproyect`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL DEFAULT '0',
  `pass` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `est` enum('act','bloq') COLLATE utf8_spanish2_ci DEFAULT 'act',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `pass`, `est`) VALUES
(102017, 'qiFUkAVmdX6zvJNx7Y4nw1yCzIBHBD4l/rJt+LAJhePwkeRJWVPYM0iNiaPt7fcoAF8B1okpOLHvHuvd4/mA9A==', 'act');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usu_proyect`
--

CREATE TABLE IF NOT EXISTS `usu_proyect` (
  `id-us` int(11) NOT NULL DEFAULT '0',
  `id-proyect` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id-us`,`id-proyect`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
