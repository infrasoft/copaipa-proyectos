<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
defined('BASEPATH') OR exit('Acceso no permitido');

/**
 * Clase creada para el manejo de proyectos
 */
class Proyectos extends CI_Controller 
{
	private $data = array(
		"head" => "",
		"mje" =>"");
	
	function __construct()
    {
		parent::__construct();
        $this->load->database('proyectos', TRUE);
        $this->load->model('proyectos_mdl');
        $this->load->model('detalle_proy_mdl'); 
        		
		$this->load->helper('file');
        
        $this->load->library('sql_descripcion_tabla_aportes');
	}
    
    
	//lista de proyectos realizados
	public function lista() 
	{
		if ($this->session->userdata("log")==TRUE)
		{	
		$this->load->view('head');
        $this->load->view('header-main');  
		$buscar = $this->input->post("buscar");
		if ($buscar != NULL)
		{
			$sent = "nombre LIKE '%".$buscar.
			"%' OR creacion LIKE '%".$buscar.
			"%' OR comitente LIKE '%".$buscar."%' ORDER BY idproyect DESC";
		}
		else
		 {
			$sent = "1 ORDER BY idproyect DESC";
		 }
        $row = $this->proyectos_mdl->consulta_proyect($sent);
        
        $data_proy = array(
            'error' => "" ,
            'proyectos' => $row,
			'cantidad_reg' =>$row->num_rows());
        
        $this->load->view('menu');            
        $this->load->view('frontend/lista_proy',$data_proy);
        $this->load->view('footer');
		}
		 else 
		{
			redirect("/seguridad/login","refresh");
		}
	}
	
	//genera un nuevo proyectos
	public function nuevo($mje=0) 
	{
	    if ($this->session->userdata("log")==TRUE)
        {
            switch ($mje) 
            {
                case 1:
                    $data["mje"] = "Se intento subir un archivo en formato no permitido,
                             por favor intentelo nuevamente";
                   break;
                case 2:
                    $data["mje"] = "Se intento cargar un item duplicado";
                   break;
                case 3:
                    $data["mje"] = "Creando un nuevo proyecto";
                   break;
                default:
                    $data["mje"]= "";
                    break;
            }	       
            
            
		      $this->load->view('head2');
              $this->load->view('header-main');	
		      $this->load->view('menu');   
              if (!isset($_SESSION["idproyect"]) 
                    Or ($_SESSION["idproyect"]==0)
                    Or ($mje==3))
			  {
					$_SESSION["idproyect"]=0;
			  }
			  else
			  {
			  	  $data["consulta"] = $this->detalles_acumulados(); 
				  $data["mje"]="";
				  $this->load->view('frontend/datos_proy',$data);
			  }   
              $this->load->view('frontend/nuevo_proy',$data);
              $this->load->view('footer');
        }
         else 
        {
            redirect("/seguridad/login","refresh");
        }
	}
    
        
    //genera los item de cada proyectos
    private function detalles_acumulados()// aqui tengo que completar
    {
    	if (!isset($_SESSION["idproyect"]))
		{
			$_SESSION["idproyect"]=0;
		}
        $consulta  = $this->detalle_proy_mdl->consulta(
                                        "id_proyecto=".$_SESSION["idproyect"]);
        /*foreach ($consulta as $row) // aqui me quede 
        {
            $this->get_tarea($row->certificacion, $row->subtarea, $row->id_categoria)
        }       */                                 
        
        return $row = $consulta->result();
    }
    
    //confirma los proyectos cargados y emite un presupuesto
    public function confirma()
    {
        if ($this->session->userdata("log")==TRUE)
        {
        	$this->load->view('head');
        	$this->load->view('header-main');   
        	$this->load->view('menu');
                
        	$certificacion = $this->input->post('certificacion');
        	$subtarea = $this->input->post('subtarea');    
        	$cat = substr($subtarea,strpos($subtarea, "-")+1);
        	$subtarea = substr($subtarea,0,strpos($subtarea, "-"));              
        
	        $data = array(			
				"certificacion" =>$certificacion,
				"subtarea" => $subtarea,
				"subcategoria" => $cat			
			);
        
			if (($certificacion == null) or ($subtarea == 0)  or ($cat == 0)
            	or ($certificacion == 0) or ($subtarea == null)  or ($cat == null))
			{
				redirect("/proyectos/nuevo","refresh"); 
			}
		    
        
        	$this->sql_descripcion_tabla_aportes->condicion = "id_tarea=".$certificacion.
            							                " AND id_subtarea=".$subtarea.
														" AND id_subcategoria=".$cat; 
        	$row = $this->sql_descripcion_tabla_aportes->consultaSQLbasicaRow(); 
           
        	$data["certificacion_detalle"] = utf8_encode($row["tarea"]);
        	$data["subtarea_detalle"] = utf8_encode($row["subtarea"]);
        
       		//armando la tabla        
			$this->load->library("sql_importes_tabla_aportes"); 
			$this->sql_importes_tabla_aportes -> condicion="id_tarea=".$certificacion.
													" AND id_subtarea=".$subtarea.
                                                    " AND id_subcategoria=".$cat;
        
			$consulta = $this->sql_importes_tabla_aportes -> consulSQLbasica();
			$list = ""; 
			while ($row = $consulta->fetch_assoc())
			{		    
				$list .= "<tr>			                
							<td>
								".$row["rango"]."
							</td>
							<td>
								".$row["detalle_importe"]."
							</td>
					  </tr>";	
			}
				
			$data["tabla"] = "<table class='table table-striped'>
	   							<tr class='active'>
	   								<td colspan='3'>
	   			<b>". $data["certificacion_detalle"]." ".$data["subtarea_detalle"] ."</b>
	   								</td>	
	   							</tr>
	   							<tr>	       
	   								<td>
	   									<b>Categorías</b>
	   								</td>
	   								<td>
	   									<b>Mensura Urbana</b>
	   								</td>
	   							</tr>".$list."	
	   				</table>";
		
        	$data["consulta"] = $this->detalles_acumulados();
        	if ($cat != null)
        	{
        		if ($_SESSION["idproyect"]==0)
        		{
					$this->load->view('frontend/confirmar_proy',$data);
				} 
				else
		    	{
		    		$data["mje"] = "";
		    		foreach ($data["consulta"] as $i) //comprobar la no existencia de datos
         			{
             			if (($i->certificacion == $certificacion)
                    	&& ($i->subtarea == $subtarea)
                    	&& ($i->id_categoria == $cat)) 
             			{
                			$data["mje"] = "Se intento cargar datos duplicados";
             			}
         			}                	
					$this->load->view('frontend/datos_proy',$data);
					if ($data["mje"]=="")
					{
						$this->load->view('frontend/nuevo_item',$data);
					}                	
				}			 
         	}
         	else
         	{  //cargar subtipo y datos requeridos
           		redirect("/proyectos/nuevo","refresh");    
         	}        
        
          	$this->load->view('footer');
         }
         else //seguridad
        {
            redirect("/seguridad/login","refresh");
        }
    }
    
    public function cargar_proyecto()
    {    	
     	$mje="";   
        if ($_SESSION["idproyect"]== 0)
        {
            //captando los datos
    		$nombre = $this->input->post('nombre');
        	$catastro = $this->input->post('catastro');           
        	$comitente = $this->input->post('comitente');
        	$destino = $this->input->post('destino');
        	date_default_timezone_set("America/Argentina/Salta");
        	$carga = array(
            	"nombre"=>$nombre,
            	"catastro"=>$catastro,
            	"creacion"=> date("o-m-d"),
            	"comitente"=>$comitente,
            	"destino" =>$destino); 
			
			if($this->proyectos_mdl->nuevo_proyect($carga))
       		{
            	$data["mje_proy"] = "<p>Proyecto Creado correctamente</p>";
				$consulta = $this->proyectos_mdl->consulta_proyect("nombre='".$nombre.
											"' AND catastro=".$catastro.
											" ORDER BY creacion DESC"); 
				$this->datos_proyectos($consulta);					
		        $dir = "../files/".$_SESSION["idproyect"]."/";
		        if(mkdir($dir, 0777,TRUE))
				{
					die("Fallo en la creacion del directorio");
				}				
        	}
			else
        	{
        	    $data["mje_proy"] = "<p>Fallo en la creacion del proyecto</p>";
				redirect("/proyectos/confirma","refresh");
        	}	
         }
           
        	  
               $certificacion = $this->input->post('certificacion');
               $subtarea = $this->input->post('subtarea');    
               $cat = $this->input->post('idsubcategoria');        
               $cantidad = $this->input->post('cantidad'); 
        	   
			    //subir archivo
        	    $dir = "../files/".$_SESSION["idproyect"]."/";        		
                $config['upload_path'] = $dir;
                $config['file_name'] = $_SESSION["idproyect"].
                                       "-".$certificacion.
                                       "-".$cat;
                $config['remove_spaces']=TRUE;
				$config['overwrite'] = TRUE;				
                $config['allowed_types'] = "jpg|jpeg|cad|dwf";
                $config['max_size'] = "50000";
                $config['max_width'] = "2000";
                $config['max_height'] = "2000";
                
                $this->load->library('upload', $config);
        		$this->upload->initialize($config);
				
                if (!$this->upload->do_upload('archivo'))
                 {          //*** ocurrio un error
                    $data['mje'] = $this->upload->display_errors();
                    //echo $this->upload->display_errors();
                    redirect("/proyectos/nuevo/1","refresh");
                    //return;
                 }                 

                $data['uploadSuccess'] = $this->upload->data();      
        
		 
        
        $this->load->view('head');
        $this->load->view('header-main');   
        $this->load->view('menu');
       
           
         $calculo = $this->calculadora($certificacion,$subtarea,$cat,$cantidad); 
         $data["consulta"] = $this->detalles_acumulados();         
         foreach ($data["consulta"] as $i) //comprobar la no existencia de datos
         {
             if (($i->certificacion == $certificacion)
                    && ($i->subtarea == $subtarea)
                    && ($i->id_categoria == $cat)) 
             {
                redirect("/proyectos/nuevo/2","refresh");
             }
         }
         
         $this->load->model('detalle_proy_mdl');
         $vec = array('id_proyecto' =>$_SESSION["idproyect"],
                      'certificacion'=>$certificacion,
                      'subtarea'=>$subtarea,
                      'id_categoria'=>$cat,
                      'cantidad'=>$cantidad,
                      'costo'=>$calculo,
                      'arch'=>$_SESSION["idproyect"]."-".$certificacion.
                                       "-".$cat); 
          if($this->detalle_proy_mdl->alta($vec ))
          {
              $aux = $_SESSION["total"] + $calculo;
              $mje = "item agregados correctamente";
			  $data_proy = array("total"=> $aux);
			  $this->proyectos_mdl->modifica_proyect($data_proy,$_SESSION["idproyect"]);
          }
          else
          {
              $mje = "Fallo en la carga de item";
           }
             
        $data["consulta"] = $this->detalles_acumulados();
        $data = array(
        	"idproyect"=>$_SESSION["idproyect"],
            "nombre"=>$_SESSION["nombre_proy"],
            "catastro"=>$_SESSION["catastro"],
            "creacion" => $_SESSION["creacion"],
            "certificacion" =>$certificacion,
            "subtarea" => $subtarea,
            "subcategoria" => $cat,
            "cantidad" => $cantidad,            
            "comitente" => $_SESSION["comitente"],
            "destino" => $_SESSION["destino"] ,
            "mje" =>$mje,
            "consulta"=>$this->detalles_acumulados()
            );
        
           $this->load->view('frontend/datos_proy',$data);             
         
        $this->load->view('footer');
    }
	
    // elimina un item del proyecto
    public function eliminar_item($id_proyect = 0, $certificacion = 0, $id_subtarea = 0,$id_subcategoria)
    {
        if ($this->session->userdata("log")==TRUE)
        {
            if (($id_proyect != 0) && ($certificacion != 0) && ($id_subtarea != 0) && ($id_subcategoria != 0))  
            {
                $vect = array(
                "id_proyecto"=>$id_proyect,
                "certificacion"=>$certificacion,
                "subtarea"=>$id_subtarea,
				"id_categoria"=>$id_subcategoria);
                if($this->detalle_proy_mdl->borrar($vect))
                {
                        $data["mje"] = "Datos eliminados correctamente";
                }
                else
                {
                        $data["mje"] = "Error en la eliminacion de datos";
                }
				$data["consulta"] = $this->detalles_acumulados();
                $this->load->view('head');
                $this->load->view('header-main');   
                $this->load->view('menu');
                $this->load->view('frontend/datos_proy',$data); 
                $this->load->view('footer');
            }
            else
            {
                redirect("/proyectos/confirma","refresh");
            }
        
        }
         else 
         {
            redirect("/seguridad/login","refresh");
         }
    }
    
	//modifica los datos de un proyecto
	public function modifica($idproyect)
	{
		
	   	if ($this->session->userdata("log")==TRUE)
       	{
			$this->load->view('head');
       		$this->load->view('header-main');   
       		$this->load->view('menu');			
			$consulta = $this->proyectos_mdl->consulta_datos($idproyect);
			$this->datos_proyectos($consulta);
			$data["consulta"] = $this->detalles_acumulados();
			$data["mje"] = "";
			$this->load->view('frontend/datos_proy',$data);
			$this->load->view('frontend/modifica_proy',$data); 
			$this->load->view('footer');
	     }
    	 else 
        {
            redirect("/seguridad/login","refresh");
	     }
		
	}
	
		
	//realiza el cambio de password
	public function password() //falta testear
	{
	    if ($this->session->userdata("log")==TRUE)
        {
		  $pass = $this->input->post("oldpassword");
		  $new_pass = $this->input->post("password");
		  $passconf = $this->input->post("passconf");
		  
		  $this->form_validation->set_rules('password', 'Password', 'required');
          $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
		  
		  
		  if ($this->form_validation->run() == TRUE)
		  {
			  if ( ($pass != null) && ($new_pass != null) && ($new_pass == $passconf))
		  	  {
				$this->load->model('user');
				if($this->user->changepassword($new_pass,$pass))
            	{
                	$this->data["mje"] = "Cambios de password realizado correctamente";
            	}      
                else
                {
                   $this->data["mje"] = "El password no es correcto";
                }       
		  	  }
			  else
			  {
				  $this->data["mje"] = "fallo en los datos de verificacion";
			  }
		  }		  
		  	  $data["otros"] = "<script src=\"".base_url()."media/js/form.js\"></script>";
	    	  $this->load->view('head3',$data);
           	  $this->load->view('header-main');
			  $this->load->view('menu');   
        	  $this->load->view('shared/changepassword',$this->data);
			  $this->load->view('footer');
		   
	     }
         else 
         {
            redirect("/seguridad/login","refresh");
         }
     }

	//pasa los datos del proyecto a la seccion
	private function datos_proyectos($consulta=null)
	{		
				$_SESSION["idproyect"] = $consulta->row('idproyect');
				$_SESSION["nombre_proy"] = $consulta->row('nombre');
				$_SESSION["catastro"] = $consulta->row('catastro');
				$_SESSION["creacion"] = $consulta->row('creacion'); 
				$_SESSION["comitente"] = $consulta->row('comitente');
				$_SESSION["destino"] = $consulta->row('destino');
				$_SESSION["total"] = $consulta->row('total');
	}
    
    //realiza el calculo de las tareas - sub tareas - descuentos - etc
    private function calculadora($certificacion=0,$subtarea=0,$cat=0,$cantidad=0)
    {
        //calculo de cantidad                    
        $this->load->library('sql_detalle_tabla_aportes');      
        $this->sql_detalle_tabla_aportes->condicion = 
                    " tacrt_id=".$certificacion." AND tasubcrt_id=".$subtarea.
                    " AND tatarsubcrt_id=".$cat;
        $consulta = $this->sql_detalle_tabla_aportes->consulSQLbasica();
        
        $calculo = 0;     
         while ($row = $consulta->fetch_assoc())
         {                       
                 if ($row["tacrtimportes_fijo"] == 0 )//tiene un min y max
                 {
                     if(($row["tacrtimportes_desde"] <= $cantidad) && 
                           ($cantidad <= $row["tacrtimportes_hasta"]))
                     {//importe costo variable
                        $variable = $cantidad  - ($row["tacrtimportes_desde"]-1);
                        $calculo = $row["tacrtimportes_importeFijo"] + 
                              ($row["tacrtimportes_importevariable"]*
                              $variable);                            
                     } 
                 }
                  else
                 {//importe costo fijo
                    $calculo = $row["tacrtimportes_importeFijo"];
                 }
         }
         //verificando si existe el descuento
         $this->load->library('sql_subcertificaciones_tabla_aportes');
         $this->sql_subcertificaciones_tabla_aportes->condicion =
            "tacrt_id=".$certificacion." AND tasubcrt_id=".$subtarea.
            " AND tatarsubcrt_id=".$cat;
         $consulta = $this->sql_subcertificaciones_tabla_aportes->consultaSQLbasicaRow();
         if($consulta["tatarsubcrt_descuentoObligatorio"] == 1)
         {
             $calculo = $calculo*$consulta["tatarsubcrt_porcDescuento"]/100;
         }// sin probar
        
        return $calculo;
    }

	
	//realiza la consulta de un proyecto
	public function consulta()
	{
		$this->load->view('head2');
        $this->load->view('header-main');   
        $this->load->view('frontend/consulta_proy');		
        $this->load->view('footer');
	}
	
	
}

?>