<?php
/***************************************************
	       http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/    
defined('BASEPATH') OR exit('Acceso no permitido');
/**
 *  Clase para el manejo de la seguridad del sistema
 */
class Seguridad extends CI_Controller
{
    public $data = array(        
        'nombre'=> "",
        'login' => TRUE,
        'intentos' => 0,
        'error' => "",
		'log' => FALSE);	
        
	function __construct()
    {
		parent::__construct();
        
        $this->load->helper('form','url');
        
        $this->load->library('form_validation','session');
        
        $this->load->database('proyectos', TRUE);
		
		$this->load->view('head');
        $this->load->view('header-main');
	}
    
    //muestra el formulario de login con los correspondientes errores
    public function index()
    {
        $this->load->view('head');
        $this->load->view('header-main');        
        $this->load->view('shared/login', $this->data);
        $this->load->view('footer');
    }
    
    //muestra el formulario de login con los correspondientes errores
    public function login()
    {
        $this->load->view('head');
        $this->load->view('header-main');        
        $this->load->view('shared/login', $this->data);
        $this->load->view('footer');
    }
    
    //funcion login y su correspondiente verificacion
    function verificar()
    {
        $mail = $this->input->post('email');
        $pass =$this->input->post('password');                
        //echo "email: ".$mail." password:".$pass;
		
        if($mail == null && $pass == null)
        {
            $this->data['error'] = "Parametros no completados";
            $this->load->view('shared/login',$data);
        }
        else
        {
        	$this->form_validation->set_rules('email','e-mail','required|valid_email');
			$this->form_validation->set_rules('password','password','required');
			            
			if ($this->form_validation->run()==FALSE)
			{				
				$this->data['error'] = "Ingreso de datos erroneos";
            	$this->load->view('shared/login',$data);
			}
			else
			{
				$this->load->model('user'); 
            	$verificar = $this->user->login_mod($mail, $pass); 
				if (isset($_SESSION['intentos'])) 
				{
					$_SESSION['intentos']= 0;
				}
				$intentos = 5 - $_SESSION['intentos']; 
				switch ($verificar)
				 {
				 	case 0:	                        		 	    
						$this->data['error'] = "Email no registrado. Solo restan ".
											$intentos." intentos";                        
            			$this->load->view('shared/login',$this->data);
					break;
						
					case 1:					    
						$this->data['error'] = "Datos Incorrectos. Solo restan ".$intentos.
												" intentos";                        
            			$this->load->view('shared/login',$this->data);
					break;
					
					case 2: //login correcto
						$_SESSION['intentos']=0;                        
						redirect("/proyectos/lista","refresh");
					break;									
				}
			}			
             				           
        }
        $this->load->view('footer');        
    }

    //elimina la seccion del usuario
    public function logout() //sin terminar
    {
        session_destroy();
        $this->load->view('head');
        $this->load->view('header-main');
        $this->load->view('shared/logout');
        $this->load->view('footer');
    }

    //realiza el cambio de password
    public function newpassword() //verificar la seguridad
    {
    	if (($this->session->email != "")
			&& ($this->session->log == TRUE))
		{
			$this->load->view('head');
       		$this->load->view('header-main'); 
        	$this->load->view('shared/changepassword');
        	$this->load->view('footer');
		}
		 else
	    {
			redirect("/seguridad/","refresh");
		}   
    }    
}

?>