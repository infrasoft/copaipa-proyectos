<?php
/**********************************************
 *****Libreria para acceso a base de datos*****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 
 class Ajax extends CI_Controller
 {
    function __construct()
    {
        parent::__construct();
        $this->load->library('sql_descripcion_tabla_aportes');
    }
    
    //muestra la lista de subtareas
    public function subtareas($tarea)
    {
       //$tarea = $this->input->post('elegido');
                                if (!isset($tarea)) 
                                {
                                    $tarea = 0; 
                                }
                                $this->sql_descripcion_tabla_aportes->condicion = 
                                                "id_tarea=".$tarea.
                                                " ORDER BY subtarea"; 
                                $consulta = $this->sql_descripcion_tabla_aportes->consulSQLbasica();
                                $content = "<select class='form-control' name='subtarea' id='subtarea' onchange='showCat(this.value)' required>";
                                $content .= "<option value='0-0'>Seleccionar SubTarea</option>";
                                while($row = $consulta->fetch_assoc())
                                {
                                    $content .= utf8_encode("<option value=\"".$row["id_subtarea"]."-".$row["id_subcategoria"]."\"> ".
                                    							$row["subtarea"]."</option> \n");   
                                }
                                
                                 echo $content."\n </select>";
                                   
    }

	//muestra la lista de subtareas con la correspondiente unidad
    public function subtareas_completa($tarea)
    {
       //$tarea = $this->input->post('elegido');
                                if (!isset($tarea)) 
                                {
                                    $tarea = 0; 
                                }
                                $this->sql_descripcion_tabla_aportes->condicion = 
                                                "id_tarea=".$tarea.
                                                " ORDER BY subtarea"; 
                                $consulta = $this->sql_descripcion_tabla_aportes->consulSQLbasica();
                                $content = "<select class='form-control' name='subtarea' id='subtarea' onchange='showCat(this.value)' required>";
                                $content .= "<option value='0-0'>Seleccionar SubTarea</option>";
                                while($row = $consulta->fetch_assoc())
                                {
                                    $content .= utf8_encode("<option value=\"".$row["id_subtarea"]."-".$row["id_subcategoria"]."\"> ".
                                    							$row["subtarea"]."</option> \n");   
                                }
                                
                                 echo $content."\n </select>";
                                   
    }

    //muestra las tablas de consultas
    public function tablaconsulta($certificacion=0, $subtarea=0, $cat=0)
    {      
		
		//armando la tabla        
		$this->load->library("sql_importes_tabla_aportes"); 
		$this->sql_importes_tabla_aportes -> condicion="id_tarea=".$certificacion.
													" AND id_subtarea=".$subtarea.
                                                    " AND id_subcategoria=".$cat;
        
		$consulta = $this->sql_importes_tabla_aportes -> consulSQLbasica();
		$list = ""; 
		while ($row = $consulta->fetch_assoc())
		{		    
			$list .= "<tr>			                
							<td class='text-left'>
								".$row["rango"]."
							</td>
							<td class='text-right'>
								$".$row["detalle_importe"]."
							</td>
					  </tr>";	
		}
		
        $this->load->library("Sql_subcertificaciones_tabla_aportes");
        $this->sql_subcertificaciones_tabla_aportes -> condicion = "tacrt_id=".$certificacion.
                                                    " AND tasubcrt_id=".$subtarea.
                                                    " AND tatarsubcrt_id=".$cat;	
        $row = $this->sql_subcertificaciones_tabla_aportes -> consultaSQLbasicaRow();
        $desc="";
        if ($row["tatarsubcrt_descripcionPorcDescuento"] != "")
        {
            $desc = "<tr ><td colspan='2'><p class='text-center'><small>".utf8_encode("Descuento: ".
                                 $row["tatarsubcrt_descripcionPorcDescuento"]).
                    "</small></p></td></tr>";
        }
        
        
		$tabla = "<table class='table table-striped'>
	   <tr class='active'>
	   		<td colspan='2'>
	   			<b>Tabla de aportes</b>
	   		</td>	
	   </tr>".$desc."
	   <tr>	       
	   		<td>
	   			<b>Categorías</b>
	   		</td>
	   		<td>
	   			<b>Mensura Urbana</b>
	   		</td>
	   </tr>".$list."	
	   </table>";
	   echo $tabla;
    }
 
 	//muestra las unidades consultadas
 	 public function muestra_unidad($certificacion=0, $subtarea=0, $cat=0)
	 {
	 	//armando la consulta
		 $this->load->library("sql_descripcion_tabla_aportes"); 
		$this->sql_descripcion_tabla_aportes -> condicion="id_tarea=".$certificacion.
													" AND id_subtarea=".$subtarea.
                                                    " AND id_subcategoria=".$cat;
        
		$consulta = $this->sql_descripcion_tabla_aportes -> consulSQLbasica();
		while ($row = $consulta->fetch_assoc())
		{
			echo $row["unidad_de_medida"];
		}
	 }

}    
 
 	
 ?>
