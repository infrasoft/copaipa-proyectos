<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
defined('BASEPATH') OR exit('Acceso no permitido');
/**
 *  Clase para el manejo de Consultas
 */
class Consulta extends CI_Controller 
{

	function __construct()
    {
	   	parent::__construct();
        
        $this->load->library('sql_descripcion_tabla_aportes');
        
        $this->load->helper('formato_texto'); 
	}
    
    //realiza el calculo de las tareas - sub tareas - descuentos - etc
    private function calculadora($certificacion=0,$subtarea=0,$cat=0,$cantidad=0)
    {
        //calculo de cantidad                    
        $this->load->library('sql_detalle_tabla_aportes');      
        $this->sql_detalle_tabla_aportes->condicion = 
                    " tacrt_id=".$certificacion." AND tasubcrt_id=".$subtarea.
                    " AND tatarsubcrt_id=".$cat;
        $consulta = $this->sql_detalle_tabla_aportes->consulSQLbasica();
        
        $calculo = 0;     
         while ($row = $consulta->fetch_assoc())
         {                       
                 if ($row["tacrtimportes_fijo"] == 0 )//tiene un min y max
                 {
                     if(($row["tacrtimportes_desde"] <= $cantidad) && 
                           ($cantidad <= $row["tacrtimportes_hasta"]))
                     {//importe costo variable
                        $variable = $cantidad  - ($row["tacrtimportes_desde"]-1);
                        $calculo = $row["tacrtimportes_importeFijo"] + 
                              ($row["tacrtimportes_importevariable"]*
                              $variable);                            
                     } 
                 }
                  else
                 {//importe costo fijo
                    $calculo = $row["tacrtimportes_importeFijo"];
                 }
         }
         
         //verificando si existe el descuento
         $this->load->library('sql_subcertificaciones_tabla_aportes');
         $this->sql_subcertificaciones_tabla_aportes->condicion =
            "tacrt_id=".$certificacion." AND tasubcrt_id=".$subtarea.
            " AND tatarsubcrt_id=".$cat;
         $consulta = $this->sql_subcertificaciones_tabla_aportes->consultaSQLbasicaRow();
         $_SESSION["desc"] = 0;
         if($consulta["tatarsubcrt_descuentoObligatorio"] == 1)
         {
             $calculo = $calculo*(100 - $consulta["tatarsubcrt_porcDescuento"])/100;
         }
		 else
		 {
			 if ($consulta["tatarsubcrt_porcDescuento"] != 0)
			 {
				$_SESSION["desc"] = $consulta["tatarsubcrt_porcDescuento"];
			 }		 
		 }
         $calculo = round($calculo);
         //$calculo = number_format($calculo, 0, '.', ',');
        return $calculo;
    }
    
    //realiza una consulta - mostrando la tabla
    public function consulta_simple()
    {
        $this->load->view('head2');
        $this->load->view('header-main');   
        $this->load->view('frontend/consulta_proy');        
        $this->load->view('footer');
    }
    
    //realiza la consulta de un proyecto con su cantidad
    public function consulta_completa($control=0,$del=-1)
    {
        $this->load->view('head4');
        $this->load->view('header-main');   
        $cantidad = $this->input->post('cantidad');
        $certificacion = $this->input->post('certificacion');
        $subtarea = $this->input->post('subtarea');    
        $cat = substr($subtarea,strpos($subtarea, "-")+1);
        $subtarea = substr($subtarea,0,strpos($subtarea, "-"));  
        		
		
         //inicializar variables            
          
        if (isset($cantidad))        
        {
            $_SESSION["cantidad"] = $cantidad;
        }
        
        if (!isset($_SESSION["cantidad"]))
        {
            $_SESSION["cantidad"] = 0;
        }
        
        if (($del!= -1)&&($control == 2))//eliminar un elemento 
        {          
             unset($_SESSION["vector_tarea"][$del]);
        }		
		
		if ($control ==1)//reset
		{
			$_SESSION["vector_tarea"] = array();
			$_SESSION["cantidad"] = 0;
			redirect("consulta/consulta_completa/","refresh");
		}
		
		if (($control == 3)&&($del != -1))// aplicar descuento sugerido
		{			
			$vect = $_SESSION["vector_tarea"][$del];			
			
			$_SESSION["vector_tarea"][$del]["calculo"] = 
				$_SESSION["vector_tarea"][$del]["calculo"]*
				(100-$_SESSION["vector_tarea"][$del]["desc"])/100;
				
			$_SESSION["vector_tarea"][$del]["calculo2"] = 
				muestra_punto("".$_SESSION["vector_tarea"][$del]["calculo"]);// verificar
				
			$_SESSION["vector_tarea"][$del]["desc"] = "ok"; 			
			//print_r($vect);
			redirect("consulta/consulta_completa/","refresh");
		}
		
		$this->load->view('frontend/consulta_compl_proy');
		
		if (!isset($_SESSION["vector_tarea"]))
		{
			$_SESSION["vector_tarea"] = array();
		}
		
		$data["mje"]="";
        if (($subtarea != 0) && ($subtarea != null) ) //realizando las consultas correspondientes      
        {
            $row = $this->sql_descripcion_tabla_aportes->get_tarea($certificacion, $subtarea, $cat);            
            if ($row["Tipo_de_descuento"] == "")
			{
                $desc = "";
            } 
            else
			{
                $desc = ". Descuento ".$row["Tipo_de_descuento"].": ".utf8_encode($row["descuento"]);
            }
                      
            
            $calculo = $this->calculadora($certificacion,$subtarea,$cat,$cantidad);
             
           
             if (!in_array(array(
                                 "tarea" => utf8_encode($row["tarea"]),
                                 "subtarea" => utf8_encode($row["subtarea"]),
                                 "cantidad" => $cantidad.$row["unidad_de_medida"],
                                 "cantidad2" => $cantidad.$row["unidad_de_medida"],
                                 "calculo" => $calculo,
                                 "calculo2" => muestra_punto("".$calculo),
                                 "otro" => $desc
                                   ), 
                                $_SESSION["vector_tarea"])) 
                    {                         
                         	if ($_SESSION["desc"]!=0) 
                         	{
								$_SESSION["vector_tarea"][] = array(
                                            "tarea" => utf8_encode($row["tarea"]),
                                            "subtarea" => utf8_encode($row["subtarea"]),
                                            "cantidad" => $cantidad,                                           
                                            "unidad_de_medida" =>$row["unidad_de_medida"],
                                            "calculo" => $calculo,
                                            "calculo2" => muestra_punto("".$calculo),
                                            "desc" => $_SESSION["desc"],
                                            "otro" => $desc
                                            );  
							}
							else
							{
								$_SESSION["vector_tarea"][] = array(
                                            "tarea" => utf8_encode($row["tarea"]),
                                            "subtarea" => utf8_encode($row["subtarea"]),
                                            "cantidad" => $cantidad,                                           
                                            "unidad_de_medida" =>$row["unidad_de_medida"],
                                            "calculo" => $calculo,
                                            "calculo2" => muestra_punto("".$calculo),
                                            "desc" => 0,
                                            "otro" => $desc
                                            ); 
							}
                                                     
                    }              
            
        }
        //print_r($_SESSION["vector_tarea"]);
        $this->load->view('frontend/mensajes_proy',$data);
        $this->load->view('footer');
    }
}

?>