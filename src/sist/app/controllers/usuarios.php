<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
defined('BASEPATH') OR exit('Acceso no permitido');

/**
 * Clase creada para el manejo de usuarios
 */
class Usuarios extends CI_Controller 
{
    
    function __construct()
    {
        parent::__construct();
    }
    
    //muestra la lista de usuarios asocisados al proyecto
    public function index()
    {
        $this->load->view('head');
        $this->load->view('header-main');   
        $this->load->view('menu');
    }
    
    //agrega usuario al proyecto
    public function agregar()
    {
        $this->load->view('head');
        $this->load->view('header-main');   
        $this->load->view('menu');
    }
    
    //elimina un usuario del proyecto
    public function elimina()
    {
        $this->load->view('head');
        $this->load->view('header-main');   
        $this->load->view('menu');
    }
}
?>