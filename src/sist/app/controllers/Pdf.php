<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
defined('BASEPATH') OR exit('Acceso no permitido');

/**
 * Clase para el manejo de pdf
 */
class Pdf extends CI_Controller
{	
	/*function __construct()
    {
		parent::__construct();
		
	}*/
    
    //genera el pdf para descarga
    public function index()
    {
        $this->load->library('Pdf_lib');        
        
        $this->pdf = new Pdf_lib();
        $this->pdf->AddPage();
        $this->pdf->Ln(15);
        $this->pdf->SetFont('Arial', 'I', 14);
        $this->pdf->Cell(190,10,'Tabla de aportes',0,1,'C');
        $cabecera = array("Certificacion", "Subtarea", "Cant","Un","Costo"); 
		if (!isset($_SESSION["vector_tarea"]))
		{
			$_SESSION["vector_tarea"] = array();
		}
		
        $this->pdf->FancyTable($cabecera, $_SESSION["vector_tarea"]);
        $this->pdf->Cell(150,10,'Total: $'.$_SESSION["total"],0,1,'C');
        $this->pdf->Output("Tabla-de-aportes.pdf", 'D');   
    }
}

?>