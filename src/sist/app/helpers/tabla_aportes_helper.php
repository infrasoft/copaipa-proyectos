<?php
/**********************************************
 *****Libreria para acceso a tablas*****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 if ( ! defined('BASEPATH')) exit('Acceso no permitido');    
 
 if (!function_exists('show_tabla'))
 {
  	function show_tabla($certificacion, $subtarea, $cat)
	{
		if(($certificacion != 0) and ($subtarea != 0) and ($cat  != 0))
		{
		$ci =& get_instance();
		$ci->load->library("sql_importes_tabla_aportes"); 
		$ci->sql_importes_tabla_aportes -> condicion="id_tarea=".$certificacion.
													" AND id_subtarea=".$subtarea.
                                                    " AND id_subcategoria=".$cat;
        
		$consulta = $ci->sql_importes_tabla_aportes -> consulSQLbasica();
		$list = ""; 
		while ($row = $consulta->fetch_assoc())
		{		    
			$list .= "<tr>			                
							<td>
								".$row["rango"]."
							</td>
							<td>
								".$row["detalle_importe"]."
							</td>
					  </tr>";	
		}
				
		$data["tabla"] = "<table class='table table-striped'>
	   <tr class='active'>
	   		<td colspan='3'>
	   			<b>Tabla de aportes</b>
	   		</td>	
	   </tr>
	   <tr>	       
	   		<td>
	   			<b>Categorías</b>
	   		</td>
	   		<td>
	   			<b>Mensura Urbana</b>
	   		</td>
	   </tr>".$list."	
	   </table>";
	}
	else
	 {
	  echo "Datos no concordantes";
	 }	
	}	   
 }


?>