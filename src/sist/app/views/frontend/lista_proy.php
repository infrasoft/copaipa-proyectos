<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
	<h1> Lista de proyectos </h1>
	<div class="text-right text-muted">
		<p> Se encontraron <?=$cantidad_reg ?> registro/s</p>
	</div>
	<div class="table-responsive">
		<table class="table table-striped">
			<tr class="active">
				<td>
					<b>Id</b>
				</td>
				<td>
					<b>Nombre</b>
				</td>
				<td>
					<b>Estado</b>
				</td>
				<td>
					<b>Creacion</b>
				</td>
				<td>
					<b>Op</b>
				</td>
			</tr>
			<?php			          
				 foreach ($proyectos->result() as $row) 
				 {
					 echo "<tr>
					           <td>".$row->idproyect.
                               "</td>
                               <td>".$row->nombre.
                               "</td>
                               <td>".$row->est.
                               "</td>
                               <td>".$row->creacion.
                               "</td>
                               <td>
                               		<a href='".base_url()."index.php/proyectos/modifica/".$row->idproyect."' title='Modificar Proyectos'>
                               			<span class='glyphicon glyphicon-pencil'></span>
                               		</a>
                               		<a href='' title='Modificar usuarios'>
                               			<span class='glyphicon glyphicon-user'></span>
                               		</a>
                               		<a href='' title='Formulario de pago'>
                               			<span class='glyphicon glyphicon-usd'></span>
                               		</a>
                               </td>
                            </tr>";
				 }						
				 
			 ?>
		</table>
		
	</div>