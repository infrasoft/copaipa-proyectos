<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

	if ($_SESSION["idproyect"] == 0)
	{
		echo "<h1>Nuevo Proyecto</h1>";
	}
	 else
	 {
		echo "<h2>Agregar nuevo Item</h2>";
	}
	
?>
        
        
        <div class="formulario">
            <?=form_open('proyectos/confirma', 
                        array('class' => "form-inline" , 'role' => "form",
                               'id'=>'nuevo_proy', 'name' =>"nuevo_proy"))?>
            
            	<div class="form-group">
            	
                <div class="form-group">
                	<label>Tipo de certificacion</label>
                	<select class="form-control" name='certificacion' id='certificacion' onchange="showUser(this.value)" required>
                		<option value="0">Seleccionar una categoria</option>
                		<option value="1">AGRIMENSURA</option>
                		<option value="2">ARQUITECTURA E INGENIERÍA</option>
                		<option value="3">AGRONOMÍA Y MEDIO AMBIENTE RURAL</option>
                		<option value="4">MEDIO AMBIENTE EN PROYECTOS Y OBRAS VARIAS</option>
                		<option value="5">TAREAS VARIAS</option>
                	</select>
                	    <br />
                	            
                	<label>Subtarea</label>  
                	<div id="txtHint"> Cargando...</div>                	
                	
                	<div id="txtabla"> </div> 
                </div>
                <hr />                
                <p class="text-danger"><?=$mje ?></p>
                           
                <button type="submit" class="btn btn-primary" name="hola" value="nuevo">
                    Agregar nuevo Item
                 </button>
                
            <?=form_close()?>
        </div>