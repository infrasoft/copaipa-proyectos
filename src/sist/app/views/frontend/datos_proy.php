<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
	<h1> Datos del proyecto</h1>
	<div class="formulario">		   
	   <p> <b>Nombre del proyecto:</b> <?=$_SESSION["nombre_proy"] ?> <b>Nº proyecto:</b><?=$_SESSION["idproyect"] ?></p>
	   <p> <b>Numero de Catastro:</b> <?=$_SESSION["catastro"] ?> </p>	 
	   <p> <b>Comitente:</b> <?=$_SESSION["comitente"] ?> </p> 
	   <p> <b>Destino:</b> <?=$_SESSION["destino"] ?> </p>   
	   
	   <table class="table table-hover table-responsive">
	       <tr class="active">
	           <td>Comitente</td>
	           <td>Sub Tarea</td>	           
	           <td>Cantidad</td>
	           <td>Costo</td>
	           <td>Op</td>
	       </tr>
	       <?php	           
	           $total = 0;
               foreach ($consulta as $row) 
               {
                   $total += $row->costo;                   
                   echo "<tr>
                            <td>".$row->certificacion."</td>
                            <td>".$row->subtarea."</td>                            
                            <td>".$row->cantidad."</td>
                            <td> $".$row->costo."</td>
                            <td><a href='".base_url()."index.php/proyectos/eliminar_item/".
                            				$_SESSION["idproyect"]."/".
                            				$row->certificacion."/".
                            				$row->subtarea."/".
                            				$row->id_categoria."'>x</a> </td>
                        </tr>\n";
               }
                
	           ?>
	       
	   </table>	                  
       
	   <p> <b>Total a pagar:</b> <?= $total?> </p>
       <p class="text-primary"> <?=$mje?> </p>                  
       	   
	</div>