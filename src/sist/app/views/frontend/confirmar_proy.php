<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
		
	<h1> Confirmar y Subir Proyecto</h1>
	<?=$tabla ?>
	<div class="formulario">
	    <?=form_open('proyectos/cargar_proyecto', 
                        array('class' => "form-inline" , 'role' => "form",
                               'id'=>'confirmar', 'enctype' => 'multipart/form-data' ))?>
	    <div class="form-group">
	    		<label class="sr-only" >Nombre</label>
                <input type="text" class="form-control" placeholder="Ingrese el nombre de proyecto" 
                	required="Por favor ingrese el nombre de proyecto" id="nombre" name="nombre" size="60px"/>                	
                             
                    
                <label class="sr-only" >Nro de Catastro</label>	
                <input type="number" class="form-control" placeholder="Nro de Catastro" 
                	required="Por favor ingrese Nro de Catastro" id="catastro" name="catastro" size="12px"/>
                </div>	
                <hr/>
	    		
	    		<p><b>Certificacion: </b><?=$certificacion." - ".$certificacion_detalle?> 
	       			<b>Subtarea:</b> <?=$subtarea." - ".$subtarea_detalle?></p>
	       			
	    		<input type="hidden" class="hidden" name="certificacion" value="<?=$certificacion ?>" required/>
	    		<input type="hidden" class="hidden" name="subtarea" value="<?=$subtarea ?>" required/>
	    		<input type="hidden" class="hidden" name="idsubcategoria" value="<?=$subcategoria ?>" required/>
	    		<input type="hidden" class="hidden" name="cantidad" value=""/>
	    		
	    		<label class=\"sr-only\" >Cantidad </label>    
                <input type="number" class="form-control" placeholder="Cantidad" 
                      required="Por favor ingrese la cantidad" id="cantidad" 
                         name="cantidad" step="any"/>
	    		
	    		<label class="sr-only" >Comitente</label>	
                <input type="text" class="form-control" placeholder="Comitente" 
                	required="Por favor ingrese el Comitente" id="comitente" name="comitente"/>
                	
                <label class="sr-only" >Destino</label>	
                <input type="text" class="form-control" placeholder="Destino" 
                	required="Por favor ingrese el Destino" id="destino" name="destino"/>
                
                <hr /> 
                <label class="sr-only" >Subir</label>   
                <input type="file" class="form-control" placeholder="Subir" 
                    required="Por favor ingrese un archivo" id="archivo" name="archivo"/>
                <p class="text-muted"><small>Formatos Permitidos: .cad .dwf .jpg </small></p> 
                
                <input type="submit" class="btn btn-primary" name="enviar" value="Subir Proyecto"/>   
                </div>
                <hr />
		<?=form_close()?>
		
	</div>