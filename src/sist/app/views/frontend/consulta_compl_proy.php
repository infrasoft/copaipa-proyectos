<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
        
        <h1>Tabla de aportes</h1>
        <div class="formulario">
            <?=form_open('', 
                        array('class' => "form-inline" , 'role' => "form",
                               'id'=>'nuevo_proy', 'name' =>"nuevo_proy"))?>
             <div class="form-group">            	
                 
               
                	<label>Tipo de certificacion</label>
                	<select class="form-control" name='certificacion' id='certificacion' onchange="showUser(this.value)" required>
                		<option value="0">Seleccionar una categoria</option>
                		<option value="1">AGRIMENSURA</option>
                		<option value="2">ARQUITECTURA E INGENIERÍA</option>
                		<option value="3">AGRONOMÍA Y MEDIO AMBIENTE RURAL</option>
                		<option value="4">MEDIO AMBIENTE EN PROYECTOS Y OBRAS VARIAS</option>
                		<option value="5">TAREAS VARIAS</option>
                	</select>
                	    <br />
                	            
                	<label>Subtarea</label>  
                	<div id="txtHint"> Cargando...</div><br/> 
                	
                	 <label class="sr-only">Cantidad </label> 
                	 <div class="input-group">                	 	
                		<input type='numb' class='form-control' 
                      		required='Por favor ingrese la cantidad' id='cantidad' 
                         	name='cantidad' step='any'  placeholder="Cantidad"/>   
                         <span class="input-group-addon" id="txcantidad" name="txcantidad">.</span>
                      </div>                       
                	
                	
                	<hr/>      
                	<a href="<?=base_url();?>index.php/consulta/consulta_completa/1/">         	
                		<input type="button" class="btn btn-danger" name="reset" value="Nuevo cálculo"/>
                	</a>
                    <input type="submit" class="btn btn-primary" name="enviar" value="Agregar Subtarea y calcular"/>
                    <a href="<?=base_url();?>index.php/pdf/">          
                        <input type="button" class="btn btn-primary" name="imprimir" value="Imprimir" target="_blank"/>
                    </a>
                </div>
                                 
                
            <?=form_close()?>
        </div>