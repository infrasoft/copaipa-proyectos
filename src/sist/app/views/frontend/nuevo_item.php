<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
<div class="formulario">
	
    <p> <b>Certificacion:</b> <?=$certificacion."-".$certificacion_detalle ?></p>
    <p> <b>Subtarea:</b> <?=$subtarea."-".$subtarea_detalle ?></p>
    <p><b>Subcategoria:</b> <?=$subcategoria ?></p>
<?=$tabla ?>
<?=form_open('proyectos/cargar_proyecto', 
                        array('class' => "form-inline" , 'role' => "form",
                               'id'=>'confirmar', 'enctype' => 'multipart/form-data' ))?>
                <input type="hidden" class="hidden" name="certificacion" id="certificacion" value="<?=$certificacion ?>" required/>
                <input type="hidden" class="hidden" name="subtarea" id="subtarea" value="<?=$subtarea ?>" required/>
                <input type="hidden" class="hidden" name="idsubcategoria" id="idsubcategoria" value="<?=$subcategoria ?>" required/>
                <input type="hidden" class="hidden" name="cantidad" id="cantidad" value=""/>
                
                <label class="sr-only" >Cantidad </label>    
                <input type="number" class="form-control" placeholder="Cantidad" 
                      required="Por favor ingrese la cantidad" id="cantidad" 
                         name="cantidad" step="any"/>
                <label class="sr-only" >Subir</label>   
                <input type="file" class="form-control" placeholder="Subir" 
                    required="Por favor ingrese un archivo" id="archivo" name="archivo"/>
                <p class="text-muted"><small>Formatos Permitidos: .cad .dwf .jpg </small></p> 
               <input type="submit" class="btn btn-primary" name="enviar" value="Subir Proyecto"/> 
        <?=form_close()?>
        </div>