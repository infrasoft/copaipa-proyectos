<?php
/***************************************************
	       http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/    
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        
        <title>COPAIPA</title>
        <meta name="description" content="">
        <meta name="author" content="Infrasoft">

        <meta name="viewport" content="width=device-width; initial-scale=1.0">      
                
        <!-- CSS de Bootstrap -->
        <link href="<?=base_url(); ?>media/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="<?=base_url(); ?>media/css/styles.css" rel="stylesheet" media="screen">
        <link rel="shortcut icon" href="http://www.copaipa.org.ar/wp-content/uploads/2015/04/favicon.ico" title="Favicon" />
        
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
        
      <?=$otros ?>
        
    </head>

	