<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/ 
?>
    <h1>Mis Datos</h1>    
    <div class="formulario">
    	<table class="table">
    		<tr>
    			<td><b>Apellido y nombre:</b></td>
    			<td><?=$this->session->userdata('nombre') ?></td>
    		</tr>
    		<tr>
    			<td><b>Email:</b></td>
    			<td><?=$this->session->userdata('email') ?></td>
    		</tr>
    		<tr>
    			<td><b>Profesion</b></td>
    			<td><?=$this->session->userdata('profesion') ?></td>
    		</tr>
    	</table>    	
    	<hr/>
    	<h2>Cambio de password</h2>
    <?=form_open('proyectos/password', 
                        array('class' => "form-inline" , 'role' => "form",
                               'id'=>'changepass','name'=>'changepass' ))?>
        <div class="form-group">
            <label class="sr-only" for="Password">Password:</label>
            <input type="password" class="form-control" id="oldpassword" name="oldpassword"
                placeholder="Ingrese su Password"  required="Por favor ingrese su Password"/>
            <hr />
            <label class="sr-only" for="Password">Nuevo Password:</label>
            <input type="password" class="form-control" id="password" name="password"
                placeholder="Nuevo Password"  required="Por favor Nuevo Password"/>
                
            <label class="sr-only" for="Password">Confirmar Password:</label>
            <input type="password" class="form-control" id="passconf" name="passconf"
                placeholder="Confirmar Password"  required="Por favor Confirmar Password"/>
             <hr />
             <button type="submit" class="btn btn-default" onsubmit="return changepassword()">
                 Cambiar password
              </button>
        </div>
      <?=form_close()?>
      
      <p  class="text-danger"><?=$mje ?></p>
      </div>  