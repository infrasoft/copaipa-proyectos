<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/
?>
    <h1> Logout</h1>
    <div class="formulario">
        <p>Acaba de salir del sistema</p>
        <p> Para volver a acceder al sistema por favor realize 
            <a href="<?=base_url(); ?>index.php/seguridad/login">click  aqui</a></p>
    </div>