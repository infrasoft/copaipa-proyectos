<?php
/***************************************************
	       http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<title>COPAIPA</title>
		<meta name="description" content="">
		<meta name="author" content="Infrasoft">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">		
				
		<!-- CSS de Bootstrap -->
    	<link href="<?=base_url(); ?>media/css/bootstrap.min.css" rel="stylesheet" media="screen">
    	<link href="<?=base_url(); ?>media/css/styles.css" rel="stylesheet" media="screen">
    	<link rel="shortcut icon" href="http://www.copaipa.org.ar/wp-content/uploads/2015/04/favicon.ico" title="Favicon" />
    	
    	<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    	
    	<script>
		function showUser(str) 
		{
  		if (str=="") 
  		{
    		document.getElementById("txtHint").innerHTML="";
    		return;
  		}
  		if (window.XMLHttpRequest) 
  		{
    	// code for IE7+, Firefox, Chrome, Opera, Safari
    		xmlhttp=new XMLHttpRequest();
  		}
  		else
  		{ // code for IE6, IE5
    		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  		}
  		xmlhttp.onreadystatechange=function() 
  		{
    		if (this.readyState==4 && this.status==200) 
    		{
      			document.getElementById("txtHint").innerHTML=this.responseText;
    		}
  		}
  		xmlhttp.open("POST","<?=base_url(); ?>index.php/ajax/subtareas_completa/"+str, true);
  		xmlhttp.send();
		}
		
		//funcion creada para mostrar la unidad de las subcategorias
		function showCat(cat)
		{
		  var tarea=document.getElementById("certificacion").value	
		  if (cat=="") 
  		{
    		document.getElementById("txtHint").innerHTML="";
    		return;
  		}
  		if (window.XMLHttpRequest) 
  		{
    	// code for IE7+, Firefox, Chrome, Opera, Safari
    		xmlhttp=new XMLHttpRequest();
  		}
  		else
  		{ // code for IE6, IE5
    		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  		}
  		xmlhttp.onreadystatechange=function() 
  		{
    		if (this.readyState==4 && this.status==200) 
    		{
      			document.getElementById("txcantidad").innerHTML=this.responseText;
    		}
  		}
  		cat= cat.replace("-", "/"); 
  		xmlhttp.open("POST","<?=base_url(); ?>index.php/ajax/muestra_unidad/"+tarea+"/"+cat, true);
  		xmlhttp.send();
		}
</script>    	
   	</head>
