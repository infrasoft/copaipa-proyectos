<?php
/***************************************************
	       http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

?>
<header>
			
 <!-- menu -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <a class="navbar-brand" href="#"><img src="<?=base_url(); ?>media/img/LogoCopaipa.png"  class="logo"/></a>
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    
  </div>
   
  <div class="collapse navbar-collapse navbar-ex1-collapse"> 
    <ul class="nav navbar-nav navbar-right">
      <li><a href="<?=base_url(); ?>index.php/proyectos/lista/">Mis proyectos</a></li>
      <li >
        <a href="<?=base_url(); ?>index.php/proyectos/nuevo/3" >
          Nuevo 
        </a>        
       </li>
       <li>
           <a href="<?=base_url(); ?>index.php/proyectos/consulta/">Consulta</a>
       </li>
       <li>
       
        <?=form_open('proyectos/lista', 
				        array('class' => "form-inline" , 'role' => "form",
                               'id'=>'buscador' ))?>
      		<div class="form-group">      			
        		<input type="text" class="form-control" placeholder="Buscar" name="buscar" id="buscar">
      		</div>
      		<button type="submit" class="btn btn-default">Enviar</button>
      	<?=form_close()?>
             
      </li>
      <li>
      	<a href="<?=base_url(); ?>index.php/proyectos/password/">Mis Datos</a>
      </li>
      <li>
      	<a href="<?=base_url(); ?>index.php/seguridad/logout">Salir</a>
      </li>
    </ul>
  </div>  
</nav>

<p class="text-right text-success"><?=$this->session->userdata('profesion')." ".$this->session->userdata('nombre') ?></p>

		</header>		
		<!--End Menu -->	