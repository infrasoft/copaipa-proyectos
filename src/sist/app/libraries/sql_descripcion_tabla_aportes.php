<?php
/**********************************************
 *****Libreria para acceso a base de datos*****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 //version en php 5.5 - usando mysqli
 
 if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');
 
 /**
  * Clase para consulta de la tabla de aportes
  */
 class Sql_descripcion_tabla_aportes 
 {
     private $tabla = "view_descripcion_tabla_aportes";
     private $campos = "id_tarea,tarea,id_subtarea,subtarea,unidad_de_medida,descuento,id_subcategoria,Tipo_de_descuento";   
     public $condicion = "";
     private $dbhost = "186.153.132.250";
     private $dbusername = "tablaportes";
     private $dbpass = 'G_45$apo$Vw';
     private $dbname = "copaipa2";
     function __construct()
     {
         
     }
     
     //conecta la base de datos
    private function conecta()
    {
        $mysqli = new mysqli($this->dbhost, $this->dbusername, $this->dbpass, $this->dbname); 
        if($mysqli->connect_errno > 0)
        {    
              die("Imposible conectarse con la base de datos [" . $mysqli->connect_error . "]");   
        }
        return $mysqli;
    }
    
    //Realiza una consulta basica
    //$conexion, $tabla, $campos, $condicion
    public function consulSQLbasica() 
    {
        if ($this -> condicion == "") 
        {
            $query = "﻿﻿SELECT " . $this -> campos . " FROM " . $this -> tabla . ";";
        }
         else
        {
            $query = "﻿﻿SELECT " . $this -> campos . " FROM " . $this -> tabla . " WHERE " . $this -> condicion . ";";
        }   
        $t = (strlen($query)-6)* -1;/* Esto es por poblemas de codificacion*/
        $query= substr(utf8_decode($query),$t);     
        //echo  $query."<br/>"; // echo $query." ".$t;
        $mysqli = $this->conecta();     
        $consulta = $mysqli->query($query); 
        if($mysqli->errno) die($mysqli->error);             
        return $consulta;
    }
    
    // Realiza una consulta basica y devuelve el array
    public function consultaSQLbasicaRow() 
    {
        $consulta = $this -> consulSQLbasica();
        $row = $consulta->fetch_assoc(); 
        return $row;
    }
	
	//muestra el nombre de la tarea y la sub tarea
	public function get_tarea($certificacion=0, $subtarea=0, $cat=0)
	{
		$this->condicion = "id_tarea=".$certificacion.
						" AND id_subtarea=".$subtarea.
                        " AND id_subcategoria=".$cat;
		return $this->consultaSQLbasicaRow();
	}
 }

 
 
 ?>