<?php
/**********************************************
 *****Libreria para generar pdf*****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
if ( ! defined('BASEPATH')) exit('Acceso no permitido');

require_once APPPATH."/third_party/fpdf/fpdf.php";
/**
 *  Clase para la generacion de pdf
 */
class Pdf_lib extends FPDF
{	
	function __construct()
    {
		parent::__construct();
	}
    
    public function Header()
    {
        $this->Image(base_url().'media/img/copaipa.jpg',10,8,22);
        $this->SetFont('Arial','I',9);
		$this->Ln("8");
        $this->Cell(220,10,utf8_decode('Consejo Profesional de Agrimensores, Ingenieros y Profesiones Afines de Salta - General Güemes 529 - Salta'),0,0,'C'); $this->Ln();
        $this->Ln('5');        
    }
    
    // Tabla coloreada
function FancyTable($header, $data)
{
    // Colores, ancho de línea y fuente en negrita
    $this->SetFillColor(50,50,240);
    $this->SetTextColor(255);
    $this->SetDrawColor(5,0,0);
    $this->SetLineWidth(.3);
    $this->SetFont('Arial','I',8);
    // Cabecera
    $w = array(55, 108, 8, 8,8);
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
    $this->Ln();
    // Restauración de colores y fuentes
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    // Datos
    $fill = false;
    //$this->SetFont('Arial','I',7);
    foreach($_SESSION["vector_tarea"] as $i => $j)
    {
    	$this->SetFont('Arial','I',6);
        $this->Cell($w[0],6,utf8_decode($j["tarea"]),'LR',0,'L',$fill);
        $this->CellFitSpace($w[1],5,utf8_decode($j["subtarea"]),'LR',0,'L',$fill);
		 $this->SetFont('Arial','I',5);
        $this->Cell($w[2],6,$j["cantidad"],'LR',0,'R',$fill);
        $this->Cell($w[3],6,$j["unidad_de_medida"],'LR',0,'R',$fill);
        $this->Cell($w[4],6, "$".$j["calculo2"],'LR',0,'R',$fill);
        $this->Ln();
        $fill = !$fill;
    }
	
    // Línea de cierre
    $this->Cell(array_sum($w),0,'','T');
    $this->Ln();
	
}


//***** Aquí comienza código para ajustar texto *************
    //***********************************************************
    function CellFit($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='', $scale=false, $force=true)
    {
        //Get string width
        $str_width=$this->GetStringWidth($txt);
 
        //Calculate ratio to fit cell
        if($w==0)
            $w = $this->w-$this->rMargin-$this->x;
        $ratio = ($w-$this->cMargin*2)/$str_width;
 
        $fit = ($ratio < 1 || ($ratio > 1 && $force));
        if ($fit)
        {
            if ($scale)
            {
                //Calculate horizontal scaling
                $horiz_scale=$ratio*100.0;
                //Set horizontal scaling
                $this->_out(sprintf('BT %.2F Tz ET',$horiz_scale));
            }
            else
            {
                //Calculate character spacing in points
                $char_space=($w-$this->cMargin*2-$str_width)/max($this->MBGetStringLength($txt)-1,1)*$this->k;
                //Set character spacing
                $this->_out(sprintf('BT %.2F Tc ET',$char_space));
            }
            //Override user alignment (since text will fill up cell)
            $align='';
        }
 
        //Pass on to Cell method
        $this->Cell($w,$h,$txt,$border,$ln,$align,$fill,$link);
 
        //Reset character spacing/horizontal scaling
        if ($fit)
            $this->_out('BT '.($scale ? '100 Tz' : '0 Tc').' ET');
    }
 
    function CellFitSpace($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        $this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,false,false);
    }
 
    //Patch to also work with CJK double-byte text
    function MBGetStringLength($s)
    {
        if($this->CurrentFont['type']=='Type0')
        {
            $len = 0;
            $nbbytes = strlen($s);
            for ($i = 0; $i < $nbbytes; $i++)
            {
                if (ord($s[$i])<128)
                    $len++;
                else
                {
                    $len++;
                    $i++;
                }
            }
            return $len;
        }
        else
            return strlen($s);
    }
//************** Fin del código para ajustar texto *****************
//******************************************************************

    
    public function Footer()
    {
                $this->SetY(-20);
           $this->SetFont('Arial','I',8);
		   date_default_timezone_set ('America/Argentina/Salta');
           $this->Cell(0,10,utf8_decode('-El presente documento tiene carácter informativo - '.
           							date("d-m-Y")." - ".date ("h:i:s")	),0,0,"C");
           $this->Ln();
           $this->Cell(0,10,utf8_decode('  Pagina '.$this->PageNo()),0,1,'R');
    }
}

?>