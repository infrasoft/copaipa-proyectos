<?php
/**********************************************
 *****Libreria para acceso a base de datos*****
 **********************************************
 *Autor: Ariel Marcelo Diaz
 *Email: ariel@infrasoft.com.ar
 *Sitio Web: http://www.infrasoft.com.ar
 ****************************************/
 //version en php 5.5 - usando mysqli
 
 if ( ! defined('BASEPATH')) exit('No se permite el acceso directo al script');
 
 /**
  * Clase para consulta de la tabla de aportes
  */
 class Sql_detalle_tabla_aportes 
 {
     private $tabla = "view_detalle_tabla_aportes";
     private $campos = "tacrt_id,tasubcrt_id,tatarsubcrt_id,tacrtimportes_secuencia,tacrtimportes_fijo,tacrtimportes_desde,tacrtimportes_hasta,tacrtimportes_importeFijo,tacrtimportes_importevariable";   
     public $condicion = "";
     private $dbhost = "186.153.132.250";
     private $dbusername = "tablaportes";
     private $dbpass = 'G_45$apo$Vw';
     private $dbname = "copaipa2";
          
     //conecta la base de datos
    public function conecta()
    {
        $mysqli = new mysqli($this->dbhost, $this->dbusername, $this->dbpass, $this->dbname); 
        if($mysqli->connect_errno > 0)
        {    
              die("Imposible conectarse con la base de datos [" . $mysqli->connect_error . "]");   
        }
        return $mysqli;
    }
    
    //Realiza una consulta basica
    //$conexion, $tabla, $campos, $condicion
    function consulSQLbasica() 
    {
        if ($this -> condicion == "") 
        {
            $query = "﻿﻿SELECT " . $this -> campos . " FROM " . $this -> tabla . ";";
        }
         else
        {
            $query = "﻿﻿SELECT " . $this -> campos . " FROM " . $this -> tabla . " WHERE " . $this -> condicion . ";";
        }   
        $t = (strlen($query)-6)* -1;/* Esto es por poblemas de codificacion*/
        $query= substr(utf8_decode($query),$t);     
        //echo  $query."<br/>";  //echo $query." ".$t;
        $mysqli = $this->conecta();     
        $consulta = $mysqli->query($query); 
        if($mysqli->errno) die($mysqli->error);             
        return $consulta;
    }
    
    // Realiza una consulta basica y devuelve el array
    function consultaSQLbasicaRow() 
    {
        $consulta = $this -> consulSQLbasica();
        $row = $consulta->fetch_assoc(); 
        return $row;
    }
 }
 
 
 ?>