<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

if ( ! defined('BASEPATH')) exit('Acceso restringido');

/**
 * Clase creada para el manejo de archivos
 */
class Archivos extends Model 
{
    public $data = array();	
    private $tabla = "archivos";
	function __construct() 
	{
	   parent::__construct();	
	}
    
    //crea un nuevo archivo
    public function nuevo_archivo()
    {
        return $this->db->insert($this->tabla, $this->data);
    }
    
    //modifica el archivo subido
    public function update_archivo()
    {
        return $this->db->update($this->tabla, $this->data);
    }
    
    //elimina el archivo
    public function delete_archivo()
    {
        $this->db->where('id', 1);
        return $this->db->delete($this->tabla);
    }
}

?>