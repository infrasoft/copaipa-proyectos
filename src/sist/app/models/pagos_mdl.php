<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

if ( ! defined('BASEPATH')) exit('Acceso restringido');

/**
 *  Clase para el manejo de pagos
 */
class Pagos_mdl extends CI_Model 
{
	private $tabla = "pagos";
	function __construct()
    {
		parent::__construct();
	}
}

?>