<?php
/***************************************************
	       http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/    

if ( ! defined('BASEPATH')) exit('Acceso restringido');

/**
 * Model para el control de usuarios
 */
class User extends CI_Model
{
    private $table = "user";   
    private $campos = "id,pass,est";
    	
	function __construct() 
	{
		parent::__construct();        
        //$this->load->database('copaipa',TRUE);		
		
		$this->load->library('session');
		$this->load->library('encrypt');
		$this->load->library('sql_usuarios');
	}
	
	//verifica el usuario y password
	public function login_mod($email,$pass)
	{
		//verificar si el usuario esta activo
				
		$this->sql_usuarios->condicion = "email='".$email."'";
		$row =$this->sql_usuarios->consultaSQLbasicaRow();		
     
        if($row != null)
        {              
            $data['id'] = $row['id'];
            $data['email'] = $row['email'];
			$data['nombre'] = $row['nombre'];
            $data['profesion'] = $row['profesion'];
            
            if (!isset($_SESSION['intentos'])) 
            {
                $_SESSION['intentos']=0;                
            }         
            
			$this->session->set_userdata($data);
            
           	$sent = "SELECT ".$this->campos.
					" FROM ".$this->table.
					" WHERE id=".$row['id'].";";
            //echo " ".$sent." ";
						
			$query = $this->db->query($sent);			
            if ($query->num_rows() != 0) 
            {   //usuario logueado - redireccionar a proyectos - si es primera ves cambio de password        
            	if ($_SESSION['intentos']>=4)
				{//bloquear cuenta
					$data['est'] = "bloq";
					$this->db->update($this->tabla, $data);
				}
				 else 
				{
					if ($this->encrypt->decode($query->row('pass')) == $pass)
                	{
                    	$data['log'] = TRUE;
        		    	$this->session->set_userdata($data);
                    	return 2;
                	}
                	else 
                	{ //mal password
                    	$_SESSION['intentos']++;
                    	$data['log'] = FALSE;
                    	$this->session->set_userdata($data);
                    	return 1;
                	}
				}
                
            }
            else
            {   // el usuario no existe
            	$data['log'] = FALSE;
                $_SESSION['intentos']++;
        		$this->session->set_userdata($data);
                 return 1; //email correcto, mal password
            }
        }
        else 
        {   //email no registrado       
        	$data['error']="Email no registrado";
        	$data['log'] = FALSE;
        	$this->session->set_userdata($data); 	
            return 0; 
        }        
	}
    
    //realiza el cambio de password 
    public function changepassword($new_pass = "",$pass="")
    {
    	$sent = "SELECT ".$this->campos.
					" FROM ".$this->table.
					" WHERE id=".$row['id'].";";
		$query = $this->db->query($sent);
		if ($this->encrypt->decode($query->row('pass')) == $pass)
		{
			$data = array(			
				"pass"=>$new_pass);
			$this->db->where('id', $this->session->userdata("id"));
        	return $this->db->update($this->table, $data);
		}
		else
	    {
			return 0;
		}
    	
    }
}

?>