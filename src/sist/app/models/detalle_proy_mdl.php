<?php
/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

if ( ! defined('BASEPATH')) exit('Acceso restringido');

/**
 *  Clase para el manejo del detalle de los proyectos
 */
class Detalle_proy_mdl extends CI_Model
{
    private $tabla = "detalle_proy";
	private $campos = "id_proyecto,certificacion,subtarea,id_categoria,cantidad,costo,arch,est";
    function __construct()
    {
        parent::__construct();
    }
    
    //crea un nuevo detalle del proyecto
    public function alta($data)
    {
        return $this->db->insert($this->tabla, $data);
    }
    
    //modifica un detalle del proyecto
    public function modifica($data)
    {
        return $this->db->update($this->tabla, $data);
    }
    
    //consulta el detalle de los proyectos
    public function consulta($data)
    {
        if ($data == "")
        {
           $sent= "SELECT ".$this->campos." FROM ".$this->tabla.";";  
        }
         else
        {
           $sent= "SELECT ".$this->campos." FROM ".$this->tabla." WHERE ".$data.";";
        }  //echo $sent;        
        $query = $this->db->query($sent);        
        return $query ;
    }
    
    //Realiza la baja del detalle de los proyectos
    public function borrar($data)
    {
        $this->db->where($data);
        return $this->db->delete($this->tabla);
    }
}

?>