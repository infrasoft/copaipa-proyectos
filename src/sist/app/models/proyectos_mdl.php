<?php
/***************************************************
	       http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

if ( ! defined('BASEPATH')) exit('Acceso restringido');

/**
 * Clase para el manejo de proyectos
 */
class Proyectos_mdl extends CI_Model
{
	private $tabla = "proyectos";
	function __construct()
	{
		parent::__construct();	
	}
    
    //crear un nuevo proyecto
    public function nuevo_proyect($data = array())
    {
        return $this->db->insert($this->tabla, $data); 
    }
    
    //muestra la lista de proyectos buscados
    public function consulta_proyect($data = "")
    {
        if ($data == "")
        {
           $sent= "SELECT * FROM ".$this->tabla.";";  
        }
         else
        {
           $sent= "SELECT * FROM ".$this->tabla." WHERE ".$data.";";
        }  // echo $sent;        
        $query = $this->db->query($sent);        
        return $query ;
    }
    
	//realiza la consulta de datos de un proyectos
	public function consulta_datos($idproyect=0)
	{
		return $this->consulta_proyect("idproyect=".$idproyect);
	}
	
    //realiza la modificacion de los proyectos
    public function modifica_proyect($data ,$id_proyect)
    {
    	$this->db->where("idproyect",$id_proyect);
        return $this->db->update($this->tabla, $data);
    }    
}



?>