/***************************************************
           http://infrasoft.com.ar
***************************************************
Autor: Ariel Marcelo Diaz
Desarrollo: Infrasoft Servicios Informaticos
sitio web: http://infrasoft.com.ar
***************************************************/

// funcion para el control de cambio de password
function changepassword()
{
  clave1 = document.changepass.password.value;
  clave2 = document.changepass.passconf.value; 
  if (clave1 != clave2)
  {
      alert("Las claves son distintas");
      document.changepass.password.value="";
      document.changepass.passconf.value="";
      return false;
  };
}